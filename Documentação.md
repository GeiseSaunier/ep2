
## Finalidade do projeto 

Criar o famoso jogo Snake (também conhecido como "jogo da cobrinha"), respeitando algumas especificações apresentadas na descrição do mesmo.

## Uma breve descrição acerca do jogo snake

O jogo possui um painel, onde estão dispostos os componentes do jogo: a "cobrinha", a fruta, a fase em que o jogador se encontra e a pontuação, sendo esta definida de acordo com a quantidade de frutas que a cobra come. Ao iniciar o jogo, o participante deverá ficar atento as colisões, pois, caso a cobra colida com si mesmo, aparecerá uma mensagem de "GAME OVER". É importante salientar que ao participante avançar cada fase, o nível de velocidade aumenta.

## Pré-requisitos para ter acesso ao jogo

Possuir um computador com um sistema operacional disponível para uso;
Possuir um ambiente de desenvolvimento integrado - IDE para ter acesso ao código fonte compatível com a linguagem utilizada. Especificamente para esse jogo, a linguagem Java está sendo utilizada, sendo assim, uma IDE compatível, por exemplo, seria o eclipse. 
Ao ter acesso ao código fonte, busque pela tecla de execução para o jogo apresentado, identificada, na maioria das vezes, por "Run". 

## Desenvolvimento 

Linguagem utilizada: Java, através da JVM.
Ambiente de desenvolvimento: Visual Studio Code - VScode.


## Versionamento 

Utilizou-se o Gitlab para controle de versão do desenvolvimento do jogo e gerenciamento do código
fonte.